# Lambda Function with Logging and Tracing

This initiative focuses on constructing a serverless feature using AWS Lambda, crafted in Rust with Cargo Lambda, and enable logging and tracing.

## Objectives

* Add logging to a Rust Lambda function
* Integrate AWS X-Ray tracing
* Connect logs/traces to CloudWatch

## Procedures

### Initialization of Rust Project

* Kickstart your project by generating a new Cargo Lambda project with `cargo lambda new <YOUR-PROJECT-NAME>` (`serverless-rust` in my case).
* Tailor the `Cargo.toml` and `src/main.rs` to fit your project's design and needs. (In my case, I have created a simple function that returns frequency of every character in the input string.)
* In your project folder, locally test its functionality by executing `cargo lambda watch`.

### Setting up AWS

* Head to the AWS IAM Management Console to create a new IAM User for managing credentials.
* Assign the `AWSLambda_FullAccess`, `IAMFullAccess` and `AWSXRayDaemonWriteAccess` policies.
* In the `Security Credentials` tab, create an access key for API use. Ensure this key is securely stored.
* Configure your environment variables (i.e. : `AWS_ACCESS_KEY_ID` amd `AWS_SECRET_ACCESS_KEY`) for Cargo Lambda to identify the AWS account and region for deployment.
* Compile your project with `cargo lambda build --release`.
* Deploy your project using `cargo lambda deploy`.
* In the AWS console, under AWS Lambda, locate your function, click on `Configuration`, then `Permission`, and follow the link under `Role name`.

### Setting up X-Ray for Tracing

* Go to the AWS Lambda, and enable the X-Ray active tracing functionality in the `Configuration` -> `Monitoring and operations tools`

### Screenshots
* X-Ray Tracing Enabled
![alt text](screenshots/xray.png)

* CloudWatch Logs
![alt text](screenshots/cloudwatch_logs.png)

* Trace Loggings
![alt text](screenshots/trace_loggings.png)

