use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, BTreeMap};
use tracing::{info, warn, Level};
use tracing_subscriber::FmtSubscriber;

#[derive(Deserialize, Serialize)]
struct Input {
    data: String,
}

#[derive(Deserialize, Serialize)]
struct Output {
    result: BTreeMap<char, usize>,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    // Initialze the logger
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("Unable to global default");

    info!("Starting the redactr service...");

    let func = handler_fn(process_data);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn process_data(event: Input, _ctx: Context) -> Result<Output, Error> {
    let mut result = HashMap::new();

    for c in event.data.chars() {
        // Ignore non-alphabetic characters
        if c.is_alphabetic() {
            let counter = result.entry(c.to_ascii_lowercase()).or_insert(0);
            *counter += 1;
        }
    }

    // Using BTreeMap to sort the result
    let sorted_result: BTreeMap<_, _> = result.into_iter().collect();

    Ok(Output {
        result: sorted_result,
    })
}
